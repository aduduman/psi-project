#include "Modbus_gateway.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_gateway[20];

// Definition of Thread_modbus_gateway
void Thread_modbus_gateway (void const *arg)
{
	sprintf(array_gateway, "%s", "modbus_gateway");
}

// Access to the thread definition
osThreadDef (Thread_modbus_gateway, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_modbus_gateway (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_modbus_gateway), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}
