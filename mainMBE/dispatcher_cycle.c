#include "dispatcher_cycle.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_dispatcher[20];

// Definition of Thread_dispatcher_cycle
void Thread_dispatcher_cycle (void const *arg)
{
	sprintf(array_dispatcher, "%s", "dispatcher_cycle");
}

// Access to the thread definition
osThreadDef (Thread_dispatcher_cycle, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_dispatcher_cycle (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_dispatcher_cycle), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}

