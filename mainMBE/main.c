#include "main.h"
#include "cmsis_os.h" 

int main()
{
	osKernelInitialize();
	ThreadCreate_acq_cycle();
	ThreadCreate_dispatcher_cycle();
	ThreadCreate_histories();
	ThreadCreate_modbus_gateway();
	ThreadCreate_modbus_socket();
	ThreadCreate_modbus_USB();
	osKernelStart();
	return 0;
}
